#!/usr/bin/perl -w

use strict;
use FindBin;
use lib "$FindBin::Bin/"; # Permet de détecter le chemin du script, pour le lancer tranquillou de n'importe où

use ADE::Main;
use ADE::Groupes;

my $login = 'ade-etudiant';
my $passw = 'test';

my $smtp_server = 'smtp.univ.tld'; # Serveur SMTP à utiliser
my $smtp_from = 'vcal@univ.tld'; # Adresse FROM
my $smtp_to = 'vcal@univ.tld'; # Destinataire
my $smtp_subj = 'Calendrier '; # Titre qui sera complété.

# Si smtp_login est vide => SMTP non authentifié.
my $smtp_helo = 'smtp hello';
my $smtp_login = '';
my $smtp_passw = '';

my $width = 1024;
my $height = 768;

my $images_root = '/tmp/';
my $ical_root = '/tmp/';

my $edt = ADE::Main->new
	(
		'ADELogin'	=>	$login, # login ADE Campus
		'ADEPassw'	=>	$passw, # mot de passe ADE Campus
		'Proto'		=>	'http', # protocole http/https pour ADE
		'Auth'		=>	'CAS', # Authentification via ADE
		'CASUrlLogin'	=>	'https://cas.univ-tours.fr/cas/login',
		'CASUrlLogout'	=>	'https://cas.univ-tours.fr/cas/logout',
		'ImagesRoot'	=>	$images_root, # Où stocker les images ?
		'CalendarRoot'	=>	$ical_root, # Où stocker les calendriers ICS ?
		'SMTPServer'	=>	$smtp_server, # Adresse du serveur SMTP où envoyer
		'SMTPHello'	=>	$smtp_helo,  # Chaine EHLO à envoyer
		'SMTPFrom'	=>	$smtp_from, # Adresse de l'émetteur
		'SMTPTo'	=>	$smtp_to, # Adresse de destination
		'SMTPSubj'	=> 	$smtp_subj, # Sujet du mail, sera complété par le nom du .ics
		'SMTPLogin'	=>	$smtp_login, # Pour le SMTP Authentifié, login
		'SMTPPassw'	=>	$smtp_passw, # Pour le SMTP Authentifié, mot de passe
		'TempDir'	=>	'/tmp/', # Répertoire temporaire
		'TODO'		=>	"ecrire-ical", # Séparer par des ';', parmi : ecrire-image, ecrire-ical, envoyer-mail-ical
		'Print'		=>	1, # Affichage sur la sortie standard
		'NoFork'	=>	"true",
		'EDTRoot'	=>	"emploidutemps.univ-tours.fr",
		'EDTPath'	=>	"/ade",
		'MailHost'	=>	"univ-tours.fr",
	);

my $Univ = ADE::Groupes->new();

$Univ->ajouter_groupe("Univ", "Polytech'Tours_(EPU)/DI/DI_5A/DI_5A_S09/DI_5A_G1/DI_5A_G1_1", 7870 , $width, $height, "Polytech'Tours_(EPU)/DI/DI_5A/DI_5A_S09/DI_5A_G1/DI_5A_G1_1");
$Univ->ajouter_groupe("Univ", "Polytech'Tours_(EPU)/DA/M2R-MSH/Parcours_Aménagement", 7921 , $width, $height, "Polytech'Tours_(EPU)/DA/M2R-MSH/Parcours_Aménagement");
# $Univ->ajouter_groupe("Univ", "Portalis/Batiment_E_(DI)/Amphis/Turing", 6824 , $width, $height, "Portalis/Batiment_E_(DI)/Amphis/Turing");
# $Univ->ajouter_groupe("Univ", "Portalis/Batiment_E_(DI)/Amphis/Pascal", 6826 , $width, $height, "Portalis/Batiment_E_(DI)/Amphis/Pascal");

# %EPU est une école.
my %EPU = (
	"EPU"	=> $Univ
);

$edt->exec_ecole(%EPU);
