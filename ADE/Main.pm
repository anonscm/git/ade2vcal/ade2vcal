#!/usr/bin/perl -w
#
# * Copyright (c) 2006-2008, Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr>
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *     * Redistributions of source code must retain the above copyright
# *       notice, this list of conditions and the following disclaimer.
# *     * Redistributions in binary form must reproduce the above copyright
# *       notice, this list of conditions and the following disclaimer in the
# *       documentation and/or other materials provided with the distribution.
# *     * Neither the name of the Polytech'Tours nor the
# *       names of its contributors may be used to endorse or promote products
# *       derived from this software without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr> ''AS IS'' AND ANY
# * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# * DISCLAIMED. IN NO EVENT SHALL Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr> BE LIABLE FOR ANY
# * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 

use strict;
use utf8;
use Switch;
package ADE::Main;

############ Dépendances principales
use LWP::UserAgent ; # Gestion du protocole HTTP
use HTTP::Cookies ; # Nos amis les cookies.
use Unicode::String qw(utf8 latin1);
use HTML::TreeBuilder;	# Utilisé pour récupérer tous les IDs quand on veut reconstruire la conf
use File::Path; # pour mkpath()
use File::Basename; # pour basename()
use Text::Unaccent; # suppression des accents.

# Jeux de date ...
use POSIX qw(strftime mktime); # Importer strftime
use Date::Calc qw(Monday_of_Week Add_Delta_Days); #Convi++ calcul des dates
use DateTime::Format::ICal; # Formattage ISO 8601 + Gestion GMT

############ Dépendances secondaires ...
use IO::File; # Pour jouer avec les fichiers ...
use IO::Dir; # Pour les répertoires ...

# Travailler avec les images ...
# use GD;
# use GD::Text;
# use GD::Text::Wrap;
# use GD::Convert qw(gif=gif_netpbm newFromGif=newFromGif_imagemagick wbmp);

# Pour envoyer par mail en pièce jointe !
# use MIME::Lite;
# use Net::SMTP;

sub new
{
	my ($classe, %params) = @_;
	my $self = {};
	my @threads = ();

	$self->{'login'}	= $params{'ADELogin'};
	$self->{'passw'}	= $params{'ADEPassw'};
	$self->{'proto'}	= $params{'Proto'};
	$self->{'auth'}		= $params{'Auth'}; # ADE ou CAS
	$self->{'casurl-login'}	= $params{'CASUrlLogin'};
	$self->{'casurl-logout'}= $params{'CASUrlLogout'};
	$self->{'smtp-server'}	= $params{'SMTPServer'};
	$self->{'smtp-hello'}	= $params{'SMTPHello'};
	$self->{'smtp-from'}	= $params{'SMTPFrom'};
	$self->{'smtp-to'}	= $params{'SMTPTo'};
	$self->{'smtp-subj'}	= $params{'SMTPSubj'};
	$self->{'smtp-login'}	= $params{'SMTPLogin'};
	$self->{'smtp-passw'}	= $params{'SMTPPassw'};
	$self->{'images-root'}	= $params{'ImagesRoot'};
	$self->{'icals-root'}	= $params{'CalendarRoot'};
	$self->{'tmp'}		= $params{'TempDir'};
	$self->{'todo'}		= $params{'TODO'};
	$self->{'print'}	= $params{'Print'};
	# Nombre d'or : 1.618033988749894848
	$self->{'version'}	= "ADE2vCal v1.6180333988749894848";
	$self->{'nofork'}	= $params{'NoFork'};
	$self->{'forks'}	= \@threads;
	$self->{'EDTRoot'}	= $params{'EDTRoot'}; # http://edtroot/ade/
	$self->{'EDTPath'}	= $params{'EDTPath'};
	$self->{'MailHost'}	= $params{'MailHost'};

	my $ver = `perl -v|head -n 2|tail -n 1`;
	$ver =~ /This is perl, ([^\"]+) built for/;
	$ver = "Perl " . $1;
	$ver =~ s/\(\*\)//g;
	my $linux_ver = `uname -r`;
	$linux_ver =~ s/\n//g;
	$linux_ver = "Linux " . $linux_ver;
	my $script_ver = $self->{'version'};
	$self->{'UserAgent'}	= "Mozilla/4.0 (compatible; $ver; $linux_ver; $script_ver)";

	# Ordre des jours :
	# 0,1,2,3,4,5,6 => ordre "classique" (lundi, mardi, ...)
	# 0 : lundi
	# 1 : mardi ...
	# ...
	$self->{'jours'} = '0%2C1%2C2%2C3%2C4%2C5';

	bless($self, $classe);

	return $self;
}

sub exec_ecole
{
	my ($self, %Ecole) = @_;
	# A chaque école, on associe un calendrier ADE (login, pass, etc.) et un tableau symbolisant les groupes des années.
	my $th		= $self->{'forks'};

	if($self->{'nofork'} ne "true" and $self->{'auth'} eq "CAS") {
		print STDERR "L'utilisation de fork() avec le CAS est inefficace : CAS ne supporte pas les connexions trop rapides.\n";
		return 0;
	}

	$self->aff("Démarrage de " . $self->{'version'});
	$self->aff("Identifiant : " . $self->{'UserAgent'});

	my $res = $self->connect();

	if ($res > 0) {
		$self->aff("Impossible de se connecter.");
		return 0;
	} elsif ($res == -1) {
		$self->aff("Pour des raisons techniques la consultation des emplois du temps n'est pas disponible de 22H à 23H.\nMerci de votre compréhension");
		return 0;
	}
	
	foreach my $depname (keys %Ecole)
	{
		$self->aff("--- $depname ---");
		my $dep = $Ecole{$depname};
		my $annees = $dep->extract_annees;
		foreach my $annee (keys %$annees)
		{
			my $_annee = $dep->extract_annee($annee);
			foreach my $groupe (keys %$_annee)
			{
				my $groupe = $dep->extract_groupe($annee, $groupe);
				$self->retrieve_data($groupe);
			}
		}
	}

	if($self->{'nofork'} ne "true")
	{
		foreach my $processusId(@$th)
		{
			print STDERR ".";
			waitpid($processusId, 0);
		}
		print STDERR "\nTous les processus sont terminés.\n";
	}
		
	if ($self->{'auth'} eq "CAS") {
		my $deco = $self->exec_get( $self->{'casurl-logout'} );
	}
}

sub retrieve_data
{
	my ($self, $groupe) = @_;

	my @todo	= split(";", $self->{'todo'});
	my $th		= $self->{'forks'};
	my %actions	=
		(
		'ecrire-image'		=> \&write_image,
		'ecrire-ical'		=> \&write_ical,
		'envoyer-mail-ical'	=> \&send_ical_mail,
		'reponse-http'		=> \&send_vcal_http,
		);

	## Fork : lourd, mais plus simple que le multi threading.
	# It returns the child pid to the parent process, 0  to the child process, or undef if the fork is unsuccessful
	my $fork;
	if($self->{'nofork'} ne "true")
	{
		$fork = fork();
	} else {
		$fork = 0;
	}

	if($fork eq 0)
	{

		# Avoid downloading image of not needed.
		$self->get_calimage($groupe) if $self->{'todo'} =~ /image/;
		
		# get calendar id
		$self->{'calId'} = $self->getId($groupe->{'treeID'}, $groupe);
		foreach my $act (@todo)
		{
			if(exists($actions{$act})) {
				$actions{$act}->($self, $groupe);
			}
		}

		if($self->{'nofork'} ne "true")
		{
			exit(0);
		}
	} else {
		$self->aff("Processus $fork créé.");
		push(@$th, $fork);
	}
}

sub get_calimage
{
	my ($self, $groupe) = @_;
	# $groupe contient :
	# $groupe->{'treeID'} : ID du groupe
	# $groupe->{'projID'} : ID du projet
	# ---------{'width'}  : largeur de l'image
	# ---------{'height'} : hauteur de l'image
	# ---------{'image_cur'}  : code "en dur" de l'image semaine en cours
	# ---------{'image_nxt'}  : ------------------------ semaine suivante

	my $id = $self->{'calId'};
	my $semaine_actuelle = $self->calcul_semaine(); 
	my $semaine_suivante = $semaine_actuelle+1;

	##
	#http://emploidutemps.univ-tours.fr/ade/imageEt?identifier=a8a574267f9531da7fea9b5f54f6ec12&projectId=25&idPianoWeek=4&idPianoDay=0%2C1%2C2%2C3%2C4%2C5%2C6&idTree=7878%2C7879&width=1520&height=954&lunchName=REPAS&displayMode=1057855&showLoad=false&ttl=1223202701312&displayConfId=259
	# $groupe->{'displayMode'} = $1;
	# $groupe->{'ttl'} = $1;
	# $groupe->{'displayConfId'} = $2;
	#
	##

	my $url_cur = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/imageEt?identifier=" . $id . "&projectId=" . $groupe->{'projID'} . "&idPianoWeek=" . $semaine_actuelle . "&idPianoDay=" . $self->{'jours'} . "&idTree=" . $groupe->{'treeID'} . "&width=" . $groupe->{'width'} . "&height=" . $groupe->{'height'} . "&lunchName=REPAS&displayMode=" . $groupe->{'displayMode'} . "&showLoad=false&ttl=" . $groupe->{'ttl'} . "&displayConfId=" . $groupe->{'displayConfId'};
	my $url_nxt = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/imageEt?identifier=" . $id . "&projectId=" . $groupe->{'projID'} . "&idPianoWeek=" . $semaine_suivante . "&idPianoDay=" . $self->{'jours'} . "&idTree=" . $groupe->{'treeID'} . "&width=" . $groupe->{'width'} . "&height=" . $groupe->{'height'} . "&lunchName=REPAS&displayMode=" . $groupe->{'displayMode'} . "&showLoad=false&ttl=" . $groupe->{'ttl'} . "&displayConfId=" . $groupe->{'displayConfId'};

	if($id =~ /[0-9a-z]{32}/) { # Teste si $id est un MD5
		my $cal_cur = $self->exec_get($url_cur);
		my $cal_nxt = $self->exec_get($url_nxt);
	
		if ($cal_cur->is_success && $cal_nxt->is_success) {
			if( (length($cal_cur->decoded_content) > 0)
			&&  (length($cal_nxt->decoded_content) > 0) ) {
				$groupe->{'image_cur'} = $cal_cur->decoded_content;
				$groupe->{'image_nxt'} = $cal_nxt->decoded_content;
				return 1;
			} else {
				return -1;
			}
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

sub parse_to_icalevent
{
	my ($self, $data) = @_;
	
	$data =~ /<table>(.*)<\/table>/s;
	my $table = $1;
	if($table eq "")
	{
		return 0;
	}

	$table =~ s/<\/tr>/<\/tr>\n/g;

	my @array = split(/\n/, $table);

	# supprime l'entête du tableau
	my
	(
		$ligne, $jour, $mois, $annee, $heure, $minutes, $duree, $fin, $cours, $salle, $cal, $event,
		$ug, $uuid, $heure_orig, $fin_orig, $start_gmt, $end_gmt, $start_loc, $end_loc, $categorie,
		$enseignant, $fin_minutes, $duree_h, $duree_m, $t1, $t2, $duree_heures, $duree_min, $tmpVar,
		$evId, $ens_mail, $attendee
	);

	# <tr><td>05/03/2007</td><td>08:30</td><td>2h</td><td><a href="javascript:ev(45338)">Electron Numérique_CM</a></td><td>DI_1</td><td>Pascal</td></tr>
	# <tr><td>([0-9]+)\/([0-9]+)\/([0-9]+)<\/td><td>([0-9]+):([0-9]+)<\/td><td>([0-9]+\w)<\/td><td><a href="javascript:ev\([0-9]+\)">(.*)<\/a><\/td><td>.*<\/td><td>(.*)<\/td><td>(.*)<\/td><\/tr>
	
	# Nouveau ::
	# <tr><td>17/11/2007</td><td>08:00</td><td>2h30min</td><td><a href="javascript:ev(37872)">Admin. Syst_TP_Windows_G1</a></td><td>DI_5A_G1</td><td>BAUP ADRIEN</td><td>Unix A</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>
	#/<tr><td>([0-9]+)\/([0-9]+)\/([0-9]+)<\/td><td>([0-9]+):([0-9]+)<\/td><td>(.*)?<\/td><td><a href="javascript:ev\([0-9]+\)">(.*)<\/a><\/td><td>.*<\/td><td>(.*)<\/td><td>(.*)<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><\/tr>/m

	## Version 5-truc-avril2008
	# <tr><td><SPAN CLASS="value">03/26/2008</span></td><td>Séméiochir 2 (gynécologie)</td><td>Sem. du 24 /03</td><td>Mercredi</td><td>18:00</td><td>1h</td><td>M3MTSC20</td><td>Matière</td><td></td><td>100</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>D.C.E.M. 1 </td><td>PERROTIN FRANCK </td><td>TO Amphi B </td><td></td><td></td><td></td><td></td><td></td></tr>
	#
	# match ::
	#
	#Activity  	Trainees  	Trainers  	Rooms  	Equipment  	Category5  	Category6  	Category7  	Category8
	#Date	Name	Week	Day	Hour	Duration	Code	Type	Url	Size	Number of Seats	Available Seats 	Code X	Code Y	Code Z	Time Zone	Notes	meeting note	Name	Name	Name	Name	Name	Name	Name	Name
	#
	# <tr>
	# 	<td><SPAN CLASS="value">03/26/2008</span></td>	--> Activity date				--> $1/$2/$3
	# 	<td>Séméiochir 2 (gynécologie)</td>		--> Name					--> $4
	# 	<td>Sem. du 24 /03</td>				--> Week					--> --
	# 	<td>Mercredi</td>				--> Day						--> --
	# 	<td>18:00</td>					--> Hour					--> $5:$6
	# 	<td>1h</td>					--> Duration					--> $7
	# 	<td>M3MTSC20</td>				--> Code					--> --
	# 	<td>Matière</td>				--> Type (Matière/UE)				--> --
	# 	<td></td>					--> URL						--> --
	# 	<td>100</td>					--> Size					--> --
	# 	<td></td>					--> Nombre de places				--> --
	# 	<td></td>					--> Places dispos				--> --
	# 	<td></td>					--> Code X					--> --
	# 	<td></td>					--> Code Y					--> --
	# 	<td></td>					--> Code Z ("créé automatiquement" ?)		--> --
	# 	<td></td>					--> Time Zone					--> --
	# 	<td></td>					--> Notes					--> --
	# 	<td></td>					--> Meeting note				--> --
	# 	<td>D.C.E.M. 1 </td>				--> Trainees Name				--> --
	# 	<td>PERROTIN FRANCK </td>			--> Trainers Name				--> $8
	# 	<td>TO Amphi B </td>				--> Room Name					--> $9
	# 	<td></td>					--> Equipment Name				--> --
	# 	<td></td>					--> Category5 Name				--> --
	# 	<td></td>					--> Category6 Name				--> --
	# 	<td></td>					--> Category7 Name				--> --
	# 	<td></td>					--> Category8 Name				--> --
	# </tr>
	#
	# <tr><td><SPAN CLASS="value">([0-9]+)\/([0-9]+)\/([0-9]+)<\/span><\/td><td>([^\<]+)<\/td><td>.*<\/td><td>.*<\/td><td>([0-9]+):([0-9]+)<\/td><td>([^\<]+)<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>([^\<]+)<\/td><td>([^\<]+)<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><td>.*<\/td><\/tr>

	## Version 4 déc 2008
	# PREPurge :0: <tr>
	# POSTPurge :0: <tr>
	# PREPurge :1: <SPAN CLASS="value">04/02/2009</span></td>
	# POSTPurge :1: <SPAN CLASS="value">04/02/2009</span>
	# PREPurge :2: <a href="javascript:ev(51505)">ET_M�th. de R�solution</a></td>
	# POSTPurge :2: <a href="javascript:ev(51505)">ET_M�th. de R�solution</a>
	# PREPurge :3: Sem. du 02 /02</td>
	# POSTPurge :3: Sem. du 02 /02
	# PREPurge :4: Mercredi</td>
	# POSTPurge :4: Mercredi
	# PREPurge :5: 14:00</td>
	# POSTPurge :5: 14:00
	# PREPurge :6: 2h</td>
	# POSTPurge :6: 2h
	# PREPurge :7: DI_5A_S09 </td>
	# POSTPurge :7: DI_5A_S09
	# PREPurge :8: MONMARCHE NICOLAS </td>
	# POSTPurge :8: MONMARCHE NICOLAS
	# PREPurge :9: Amphi Dpt Productique </td>
	# POSTPurge :9: Amphi Dpt Productique
	# PREPurge :10: </td>
	# POSTPurge :10:
	# PREPurge :11: </td>
	# POSTPurge :11:
	# PREPurge :12: </td>
	# POSTPurge :12:
	# PREPurge :13: </td>
	# POSTPurge :13:
	# PREPurge :14: </td></tr>
	# POSTPurge :14: </tr>
	##

	## Version du 8 octobre 2010
	# PREPurge :0: <tr>
	# POSTPurge :0: <tr>
	# PREPurge :1: <SPAN CLASS="value">06/09/2010</span></td>
	# POSTPurge :1: <SPAN CLASS="value">06/09/2010</span>
	# PREPurge :2: <a href="javascript:ev(55732)">Réunion Rentrée DI 5</a></td>
	# POSTPurge :2: <a href="javascript:ev(55732)">Réunion Rentrée DI 5</a>
	# PREPurge :3: Lundi</td>
	# POSTPurge :3: Lundi
	# PREPurge :4: 11:30</td>
	# POSTPurge :4: 11:30
	# PREPurge :5: 1h</td>
	# POSTPurge :5: 1h
	# PREPurge :6: </td>
	# POSTPurge :6: 
	# PREPurge :7: </td>
	# POSTPurge :7: 
	# PREPurge :8: DI_5A_S09 </td>
	# POSTPurge :8: DI_5A_S09 
	# PREPurge :9: AUPETIT SEBASTIEN </td>
	# POSTPurge :9: AUPETIT SEBASTIEN 
	# PREPurge :10: Turing </td>
	# POSTPurge :10: Turing 
	# PREPurge :11:  </td>
	# POSTPurge :11:  
	# PREPurge :12:  </td>
	# POSTPurge :12:  
	# PREPurge :13:  </td>
	# POSTPurge :13:  
	# PREPurge :14:  </td>
	# POSTPurge :14:  
	# PREPurge :15:  </td>
	# POSTPurge :15:  
	# PREPurge :16: </td>
	# POSTPurge :16: 
	# PREPurge :17: </td></tr>
	# POSTPurge :17: </tr>
	##

	## Version du 10 septembre 2011
	# PREPurge :0: <tr>
	# POSTPurge :0: <tr>
	# PREPurge :1: <SPAN CLASS="value">09/01/2012</span></td>
	# POSTPurge :1: <SPAN CLASS="value">09/01/2012</span>
	# PREPurge :2: <a href="javascript:ev(48845)">Algo. Graphique_TD_G1</a></td>
	# POSTPurge :2: <a href="javascript:ev(48845)">Algo. Graphique_TD_G1</a>
	# PREPurge :3: Lundi</td>
	# POSTPurge :3: Lundi
	# PREPurge :4: 14:00</td>
	# POSTPurge :4: 14:00
	# PREPurge :5: 2h</td>
	# POSTPurge :5: 2h
	# PREPurge :6: </td>
	# POSTPurge :6: 
	# PREPurge :7: </td>
	# POSTPurge :7: 
	# PREPurge :8: DI_5A_G1 </td>
	# POSTPurge :8: DI_5A_G1 
	# PREPurge :9: SLIMANE MOHAMED </td>
	# POSTPurge :9: SLIMANE MOHAMED 
	# PREPurge :10: Windows B </td>
	# POSTPurge :10: Windows B 
	# PREPurge :11:  </td>
	# POSTPurge :11:  
	# PREPurge :12:  </td>
	# POSTPurge :12:  
	# PREPurge :13:  </td>
	# POSTPurge :13:  
	# PREPurge :14:  </td>
	# POSTPurge :14:  
	# PREPurge :15:  </td>
	# POSTPurge :15:  
	# PREPurge :16: </td>
	# POSTPurge :16: 
	# PREPurge :17: </td>
	# POSTPurge :17: 
	# PREPurge :18: </td></tr>
	# POSTPurge :18: </tr>
	##

	$cal = "";
	my $ical = new DateTime::Format::ICal;
	my $dt_start_gmt = DateTime->new
		(
			year => 0,
			month => 1,
			day => 1,
			hour => 0,
			minute => 0,
			second => 0,
		);

	my $dt_end_gmt = DateTime->new
		(
			year => 0,
			month => 1,
			day => 1,
			hour => 0,
			minute => 0,
			second => 0,
		);

	foreach $ligne (@array)
	{
		if($ligne =~ /<SPAN CLASS="value">/)
		{
			my @arr = split(/<td>/, $ligne);
			for(my $i = 0; $i < @arr; $i++)
			{
				# print STDERR "PREPurge :$i: " . $arr[$i] . "\n";
				$arr[$i] =~ s/<\/td>//g;
				# print STDERR "POSTPurge :$i: " . $arr[$i] . "\n";
			}
			
			## Bornes du tableau : <tr> et </tr>
			$tmpVar = $arr[1];
			if($tmpVar =~ /(\d+)\/(\d+)\/(\d+)/)
			{
				$jour = $1;
				$mois = $2;
				$annee = $3;
			} else {
				$self->aff("Impossible de lire la date");
				return 0;
			}

			$tmpVar = $arr[4];
			if($tmpVar =~ /(\d+):(\d+)/)
			{
				$heure = $1;
				$minutes = $2;
			} else {
				$self->aff("Impossible de lire l'heure");
				return 0;
			}

			$duree = $arr[5];
			$cours = $arr[2];
			## <a href="javascript:ev(4222)">Rentrée 3A</a>
			if($cours =~ /<a href="javascript:ev\(([^\"]+)\)">([^\"]+)<\/a>/ )
			{
				$cours	= $2;
				$evId	= $1;
			}

			$attendee = $arr[8];
			$enseignant = $arr[9];
			if(defined($enseignant) and $enseignant =~ /(\w+) (\w+)/)
			{
				$ens_mail = lc($2) . "." . lc($1) . "@" . $self->{'MailHost'};
			} else {
				$ens_mail = "";
			}

			if (defined($arr[10])) {
				$salle = $arr[10];
			} else {
				$salle = "";
			}
			
			# $ug = new Data::UUID;
			# $uuid = $ug->create_str();

			$heure_orig = $heure;
			# le mois commence à 0.
			$mois -= 1;
			
			if($duree =~ /(\d+)h|(\d+)min/ )
			{
				$t1 = defined($1) ? $1 : 0;
				$t2 = defined($2) ? $2 : 0;
				$fin_orig = $heure_orig + int($t1);
				$fin = $heure + int($t1);
				$fin_minutes = $minutes + int($t2);
				$duree_heures = int($t1);
				$duree_min = int($t2);
			} else {
				$fin_orig = $heure_orig;
				$fin = $heure;
				$fin_minutes = $minutes;
				$duree_heures = 0;
				$duree_min = 0;
			}

			if($fin_minutes eq 60)
			{
				$fin_minutes = 0;
				$fin += 1;
			}

			# print STDERR "Cours de '$cours' le '$jour/$mois/$annee' de '$heure:$minutes' à '$fin:$fin_minutes' avec '$enseignant' en '$salle'.\n";
			
			$dt_start_gmt->set
				(
					year => $annee,
					month => $mois+1,
					day => $jour,
					hour => $heure,
					minute => $minutes,
					second => 0,
					# time_zone => $timezone
				);

			$dt_end_gmt->set
				(
					year => $annee,
					month => $mois+1,
					day => $jour,
					hour => $heure,
					minute => $minutes,
					second => 0,
					# time_zone => $timezone
				);
			$dt_end_gmt->add(hours	=> $duree_heures,
					minutes	=> $duree_min);

			$start_gmt	= $ical->format_datetime($dt_start_gmt);
			$end_gmt	= $ical->format_datetime($dt_end_gmt);

			$categorie = $self->categorie($cours);
			$cours =~ s/_CC|_ET|_CM|_TD|_TP//g;

			if(defined($cours) and ($cours eq ""))
			{
				$cours = "N/C";
			}

			if(defined($salle) and ($salle eq ""))
			{
				$salle = "N/C";
			}

			my $evCours = $cours;
			$evCours =~ s/'//g;
			$evCours =~ s/&/ et /g;
			$evCours =~ s/://g;
			$evCours =~ s/-/ /g;
			$evCours = unac_string($self->{'charset'}, $evCours);

			$event = "
BEGIN:VEVENT
DTSTAMP:" . $start_gmt . "
DTSTART:$start_gmt
DTEND:$end_gmt
SUMMARY:" . $cours . "
LOCATION:" . $salle ." 
UID:" . $evId . "
CATEGORIES:" . $categorie . "," . $evCours;

			if(defined($enseignant) and not($enseignant eq ""))
			{
				my $evEns = unac_string($self->{'charset'}, $enseignant);
				$evEns =~ s/'//g;
				$evEns =~ s/&/ et /g;
				$evEns =~ s/://g;
				$evEns =~ s/-/ /g;
				$evEns =~ s/\s+$//g;
				$event .= "," . $evEns . "
ORGANIZER;CN=" . $evEns . ":mailto:" . $ens_mail;
			}

			if(defined($attendee) and not($attendee eq ""))
			{
				my $evAttendee = unac_string($self->{'charset'}, $attendee);
				$evAttendee =~ s/'//g;
				$evAttendee =~ s/&/ et /g;
				$evAttendee =~ s/://g;
				$evAttendee =~ s/-/ /g;
				$evAttendee =~ s/\s+$//g;
				$evAttendee =~ s/   /_/g;
				my $evAttendees = "";
				for my $_ (split(/ /, $evAttendee)) {
					my $mail = $_;
					my $cn = $_;
					$cn =~ s/_/ /g;
					$mail =~ s/_/-/g;
					$evAttendees .= "CUTYPE=GROUP;ROLE=REQ-PARTICIPANT;CN=" . $cn . ":mailto:" . lc($mail) . "@" . $self->{'MailHost'} . ';';
				}
				$evAttendees =~ s/;$//g;
				$event .= "
ATTENDEE;" . $evAttendees;
			}

			$event .= "
URL;VALUE=URI:" . $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/eventInfo.jsp?eventId=" . $evId . " 
BEGIN:VALARM
DESCRIPTION:Rappel " . $cours . "
ACTION:DISPLAY
TRIGGER;VALUE=DURATION;RELATED=START:-PT15M
END:VALARM
END:VEVENT";
			$cal .= $event;
		}
	}
	
	return $cal;
}

sub categorie
{
	my ($self, $cours) = @_;
	
	my %categories = (
		CC	=> "Contrôle Continu",
		ET	=> "Examen Terminal",
		CM	=> "Cours Magistral",
		TD	=> "Travaux Dirigés",
		TP	=> "Travaux Pratiques"
	);
	
	for my $clef ( keys %categories ) {
		if($cours =~ /$clef/) {
			return $categories{$clef};
		}
	}

	return $categories{$cours} if(defined($categories{$cours}));
	return $categories{"CM"};
}

sub getId
{
	my ($self, $groupId, $groupe) = @_;

	my @urlListe =
		(
#	"http://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/menu.jsp?on=Planning&sub=Afficher&href=/custom/modules/plannings/plannings.jsp&target=visu&tree=true",
# 	"http://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/welcome.jsp",
# 	"http://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/set_tree.jsp?href=/custom/modules/plannings/plannings.jsp&isClickable=true&showImage=true&showTabStage=true&showTabRooms=true&showTabDuration=true&displayConfId=67&showTabHour=true&showTabActivity=true&showPianoDays=true&showTreeCategory8=true&y=&showTreeCategory7=true&displayType=0&x=&showTab=true&showTreeCategory6=true&showTreeCategory5=true&showTreeCategory4=true&showTreeCategory3=true&showTreeCategory2=true&showTabTrainees=true&showTreeCategory1=true&display=true&showPianoWeeks=true&showLoad=false&showTabDate=true&changeOptions=true&target=visu",
# 	"http://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/linksSelectWeeks.jsp",
 	$self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/plannings.jsp",
# 	"http://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/bounds.jsp",
# 	"http://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/pianoDays.jsp?forceLoad=true",
# 	"http://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/pianoWeeks.jsp?forceLoad=true",
# 	"http://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/bounds.jsp",
	);

	my $sem    = $self->calcul_semaine();
	my $sem_nxt = $sem+1;

	my $tree   = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/tree.jsp?selectId=" . $groupId . "&reset=true&forceLoad=false&scroll=0";
	my $url    = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/imagemap.jsp?week=" . $sem . "width=320&height=240";
	my $bounds = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/bounds.jsp?week=" . $sem . "&reset=true";
	my $bounds_nxt = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/bounds.jsp?week=" . $sem_nxt . "&reset=true";
#	my $setopts = $self->exec_post("http://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/menu.jsp?on=Planning&sub=Afficher&href=/custom/modules/plannings/plannings.jsp&target=visu&tree=true", "showTab=true&showTabStage=true&showTabDate=true&showTabHour=true&showTabActivity=true&showTabDuration=true&showTabTrainees=true&showTabRooms=true&showImage=true&displayConfId=67&showPianoWeeks=true&showPianoDays=true&x=9&y=7&display=true&x=&y=&isClickable=true&changeOptions=true&displayType=0&showLoad=false&showTreeCategory1=true&showTreeCategory2=true&showTreeCategory3=true&showTreeCategory4=true&showTreeCategory5=true&showTreeCategory6=true&showTreeCategory7=true&showTreeCategory8=true", "application/x-www-form-urlencoded");

	my $head = "BEGIN:VCALENDAR
VERSION:2.0
CALSCALE:GREGORIAN
PROGID:-//ADE.Campus|UnivTours//NONSGML v1.0//EN
BEGIN:VTIMEZONE
TZID:/softwarestudio.org/Olson_20011030_5/Europe/Paris
X-LIC-LOCATION:Europe/Paris
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
TZNAME:CEST
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=3
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=10
END:STANDARD
END:VTIMEZONE";

	my $foot = "
END:VCALENDAR";
	
	my ($branch, $id, $pId);
	my $trainee = $self->openCategory("trainee"); # Ouvre la catégorie étudiants
	my $rooms = $self->openCategory("room"); # Ouvre la catégorie salles
	my $instructor = $self->openCategory("instructor"); # Ouvre la catégorie profs
	if($trainee and $rooms and $instructor)
	{
		$branch = $self->check($groupId); # Coche le groupe
		if($branch)
		{
			my $tmpres;
			
			foreach (@urlListe)
			{
				# $self->aff("Fetching ... $_");
				$tmpres = $self->exec_get($_);
				if(not($tmpres->is_success)) {
					$self->aff("Erreur critique sur $_.");
					return 0;
				}
			}
			
			my $bnds = $self->exec_get($bounds);
			if($bnds->is_success)
			{
				my $img_frame = $self->exec_get($url);
				if($img_frame->is_success) 
				{
					# Maintenant, on prépare la suite : on veux cocher toutes les semaines ...
					if($img_frame->decoded_content =~ /([0-9a-z]{32})/)
					{
						$id = $1;
						## projectId=24
						$img_frame->decoded_content =~ /projectId=([0-9]+)&/;
						$pId = $1;
						$groupe->{'projID'} = $pId;
						## displayMode=1057855&showLoad=false&ttl=1223203160064&displayConfId=259
						$img_frame->decoded_content =~ /displayMode=(\d+)&/;
						$groupe->{'displayMode'} = $1;
						$img_frame->decoded_content =~ /ttl=(\d+)&displayConfId=(\d+)/;
						$groupe->{'ttl'} = $1;
						$groupe->{'displayConfId'} = $2;
						# $self->aff("Found ID=$id, pId=$pId, displayMode=" . $groupe->{'displayMode'} . ", ttd=$1, displayConfId=$2");
					} else {
						$self->aff("No ID. Check that the groupid is valid !");
						return 0;
					}

					my $allWeeks = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/pianoWeeks.jsp?searchWeeks=all";
					my $req = $self->exec_get($allWeeks);
					if(not $req->is_success)
					{
						$self->aff("Impossible de cocher toutes les semaines ...");
						return 0;
					}

					my $light  = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/custom/modules/plannings/info.jsp?order=slot";
					my $light_cur = $self->exec_get($light);

					$self->{'charset'} = $light_cur->content_type_charset;
					$groupe->{'cal'} = $self->parse_to_icalevent($light_cur->decoded_content);
					# my $calendrier = $self->utf($head . $groupe->{'cal_cur'} . $groupe->{'cal_nxt'} . $foot);

					my $calendrier = $head . $groupe->{'cal'} . $foot;
					$groupe->{'vcs'} = $calendrier;
					
					return $id;
				} else {
					return 0; # Echec ouverture Frame d'affichage
				}
			} else {
				return 0;
			}
		} else {
			return 0; # Echec du groupe.
		}
	} else {
		return 0; # Echec catégorie étudiants ou salles
	}
}

sub openCategory
{
	my ($self, $name) = @_;
	my $url = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/tree.jsp?category=" . $name . "&expand=false&forceLoad=false&reload=false&scroll=0";	
	my $res = $self->exec_get($url);
	return $res->is_success;
}

sub openBranch
{
	my ($self, $branchId) = @_;
	my $url = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/tree.jsp?branchId=" . $branchId . "&expand=false&forceLoad=false&reload=false&scroll=0";
	my $res = $self->exec_get($url);
	return $res->is_success;
}

sub check
{
	my ($self, $groupId) = @_;
	my $url = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/tree.jsp?selectId=" . $groupId . "&reset=true&forceLoad=false&scroll=0";
	my $res = $self->exec_get($url);
	return $res->is_success;
}

sub calcul_semaine
{
	my ($self) = @_;

	my $drift = 0; # Décalage de semaine ?
	my $day = strftime("%w", localtime()); # Numéro du jour de la semaine
	
	if($day >= 5) { # Vendredi, Samedi, Dimanche => on passe à la semaine n+1
		$drift = 1;
	}
	
	my $sem_cur = strftime("%W", localtime()) + $drift; # Numéro de la semaine
	my $sem_start = 35; # Première semaine de cours ... (36 en vrai, mais début à 0 chez eux)
	my $sem_delta = 51 - $sem_start; # Delta entre 52 et la première semaine de cours. 

	if($sem_cur >= $sem_start) {
		return abs($sem_cur - $sem_start);
	} else {
		return ($sem_cur + $sem_delta);
	}
}

sub semaine_intervalle
{
	my ($self, $semaine) = @_;
	
	my $__week;
	my $drift = 0; # Décalage de semaine ?
	my $day = strftime("%w", localtime()); # Numéro du jour de la semaine

	if($day > 5) { #  Samedi, Dimanche => on passe à la semaine n+1
	        $drift = 1;
	}

	my $sem_cur = strftime("%W", localtime()) + $drift; # Numéro de la semaine
	my $sem_start = 35; # Première semaine de cours ...
	my $sem_delta = 51 - $sem_start; # Delta entre 52 et la première semaine de cours.

	if($semaine > $sem_delta) { # Si on a passé une année civile ;..
			$__week = abs($semaine - $sem_delta);
	} else {
	        $__week = ($semaine + $sem_start);
	}
	
	my $__year = strftime("%Y", localtime());
	my $__vendr_d = 6;
	my ($__annee_init,$__mois_init,$__lundi) = Monday_of_Week($__week+1, $__year);
	my ($__annee_final,$__mois_final,$__vendredi) = Add_Delta_Days($__annee_init,$__mois_init,$__lundi,$__vendr_d);
	
	$__lundi = sprintf("%02d", $__lundi);
	$__vendredi = sprintf("%02d", $__vendredi);
	$__mois_init = sprintf("%02d", $__mois_init);
	$__mois_final = sprintf("%02d", $__mois_final);
	
	# return ($__lundi . "-" . $__mois_init . "-" . $__annee_init . "_" . $__vendredi . "-" . $__mois_final . "-" . $__annee_final);
	return ($__annee_init . "-" .  $__mois_init . "-" . $__lundi . "_" . $__annee_final . "-" . $__mois_final . "-" .  $__vendredi)
}

sub connect
{
	my ($self) = @_;

	# Création du navigateur virtuel
	my $ua = LWP::UserAgent->new;

	$ua->agent($self->{'UserAgent'});

	# Timeout de 60 secondes, ce qui peut faire échouer nombre de connexions,
	$ua->timeout(60);

	# L'instruction magique qui fait que LWP gère les cookies tout seul comme un grand !
	my $cookie_jar = HTTP::Cookies->new();
	$ua->cookie_jar($cookie_jar);

	# Spy connection
#	$ua->add_handler ("request_send", sub { shift->dump; return } );
#	$ua->add_handler ("response_done", sub { shift->dump; return } );

	$self->{'browser'} = $ua;

	if ($self->{'auth'} eq "ADE") {
		# my $res = $self->exec_get("http://emploidutemps.univ-tours.fr/etu/");
		my $res = $self->exec_post
		(
			$self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/menu.jsp",
				"login=" . $self->{'login'} .
				"&password=" .$self->{'passw'} ,
			"application/x-www-form-urlencoded"
		);

		return -1 if ($res->decoded_content =~ /raisons techniques/);
		return 0  if ($res->decoded_content =~ /welcome.jsp/);
		if ($res->header("Location") =~ /redirectProjects/)
		{
			$res = $self->exec_get($res->header("Location"));
		} else {
			return 1;
		}
	} elsif ($self->{'auth'} eq "CAS" and $self->{'casurl-login'} =~ /^https:\/\// and $self->{'casurl-logout'} =~ /^https:\/\//) {
		my $redir = $self->exec_get($self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/menu.jsp");
		if ($redir->decoded_content =~ /name="lt" value="([^\"]+)"/) {
			my $ticket = $1;
			my $action;

			if ($redir->decoded_content =~ /action="([^\"]+);([^\"]+)"/) {
				$action = $2;
			}
			
			my $casauth = $self->exec_post(
				$self->{'casurl-login'} . ";" . $action,
					"username=" . $self->{'login'} .
					"&password=" . $self->{'passw'} .
					"&warn=false&lt=" . $ticket .
					"&_eventId=submit",
				"application/x-www-form-urlencoded"
			);

			if ($casauth->header("Location") =~ /ticket=([^\"]+)/) {
				$self->{"ticket"} = $1;
				$casauth = $self->exec_get( $casauth->header("Location") );
			} else {
				return 1;
			}
		} else {
			return 1;
		}
	}

	my $res = $self->exec_get($self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/projects.jsp");

	my $pid;
	if($res->decoded_content =~ /projectId=(\d+)/)
	{
		$pid = $1;
	}

	if($res->decoded_content =~ /SELECT NAME="projectId"/)
	{
		my @content = split(/\n/, $res->decoded_content);
		foreach(@content)
		{
			if($_ =~ /<OPTION VALUE= (\d+) >/)
			{
				$pid = $1;
			}
		}
	}

	$res = $self->exec_post
	(
		$self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/menu.jsp",
			"ticket=" . $self->{'ticket'},
			"&projectId=" . $pid ,
		"application/x-www-form-urlencoded"
	);
	
	return 0;
}

sub write_image
{
	my ($self, $groupe) = @_;
	# $chemin :: racine où enregistrer ...
	
	my $chemin = $self->{'images-root'};
	my $semaine_cur = $self->calcul_semaine();
	my $semaine_nxt = $semaine_cur+1;

	my($filename, $dir, $suffix) = fileparse($groupe->{'name'});
	
	my $fname_cur = $filename . '-' . basename($self->semaine_intervalle($semaine_cur)) . '.png';
	my $fname_nxt = $filename . '-' . basename($self->semaine_intervalle($semaine_nxt)) . '.png';
	my $rep = $chemin . '/' . $dir . '/';

	# $self->ajout_date($groupe);
	
	# Le répertoire existe ?
	if( not( -e $rep) )
	{
		print STDERR ">> Creation de $rep.\n";
		mkpath($rep . '/', {mode => 0755} );
	}
	
	print STDERR ">> Ecriture de " . $rep . $fname_cur . "\n";
	my $fImage_cur = IO::File->new($rep . $fname_cur, O_WRONLY|O_CREAT|O_TRUNC)
		or die "Impossible d'ouvrir ``" . $rep . $fname_cur . "''. Arrêt";
		
	print STDERR ">> Ecriture de " . $rep . $fname_nxt . "\n";
	my $fImage_nxt = IO::File->new($rep . $fname_nxt, O_WRONLY|O_CREAT|O_TRUNC)
		or die "Impossible d'ouvrir ``" . $rep . $fname_nxt . "''. Arrêt";

	print $fImage_cur $groupe->{'image_cur'};
	print $fImage_nxt $groupe->{'image_nxt'};

	# On ferme.
	undef $fImage_cur;
	undef $fImage_nxt;
	
	return 1;
}

sub write_ical
{
	my ($self, $groupe) = @_;
	$self->write_ical_generic($groupe, $self->{'icals-root'});
	return 1;
}

sub write_tmp_ical
{
	my ($self, $groupe) = @_;
	$self->write_ical_generic($groupe, $self->{'tmp'});
	return 1;
}

sub write_ical_generic
{
	my ($self, $groupe, $chemin) = @_;
	# $chemin :: racine où enregistrer ...
	
	my $semaine_cur = $self->calcul_semaine();
	my $semaine_nxt = $semaine_cur+1;
	my $calendrier = $groupe->{'vcs'};
	my($filename, $dir, $suffix) = fileparse($groupe->{'name'});
	
	my $ical_name = $filename . '.vcs';
	my $rep = $chemin . '/' . $dir . '/';

	# Le répertoire existe ?
	if( not( -e $rep) )
	{
		# print STDERR ">> Création de $rep.\n";
		mkpath($rep . '/', 0755);
	}
	
	print STDERR ">>> Ecriture de " . $rep . $ical_name . "\n";
	my $fIcal_name = IO::File->new($rep . $ical_name, O_WRONLY|O_CREAT|O_TRUNC)
		or die "Impossible d'ouvrir ``" . $rep . $ical_name . "''. Arrêt";
	
	binmode($fIcal_name, ":utf8"); 
	print $fIcal_name $calendrier;

	# On ferme.
	undef $fIcal_name;

	return 1;
}

sub send_ical_mail
{
	my ($self, $groupe) = @_;
	# $chemin :: racine où enregistrer ...

	my $chemin = $self->{'tmp'};
	my $ical_name = $groupe->{'name'} . '.vcs';
	my $rep = $chemin . '/' . $groupe->{'name'} . '/';
	my $name =  $rep . $ical_name;

	$self->write_tmp_ical($groupe);

	my $server	= $self->{'smtp-server'};
	my $from	= $self->{'smtp-from'};
	my $to		= $self->{'smtp-to'};
	my $subj	= $self->{'smtp-subj'} . $ical_name;

	### Create the multipart container
	my $msg = MIME::Lite->new (
		From	=> $from,
		To	=> $to,
		Subject	=> $subj,
		Type	=> 'multipart/mixed'
	) or die "Error creating multipart container: $!\n";
	
	### Add the GIF file
	$msg->attach (
		Type		=> 'text/calendar',
		Encoding	=> 'base64',
		Path		=> $name,
		Filename	=> $ical_name,
		Disposition	=> 'attachment'
	) or die "Error adding $ical_name: $!\n";

	# Nombre d'or : 1.618033988749894848
	$msg->add("X-Mailer" => $self->{'version'});
	
	my $smtp = Net::SMTP->new
	(
		Host	=> $server,
		Hello	=> $self->{'smtp-hello'},
		Timeout	=> 10
	) or die "Impossible de contacter le serveur SMTP $!\n";

	### Send the Message
	if($self->{'smtp-login'} ne '') {
		# SMTP Auth
		$smtp->auth($self->{'smtp-login'}, $self->{'smtp-passw'})
			or die "Impossible de s'authentifier\n";
	}

	$smtp->mail($from) 
		or die "Problème avec la commande MAIL From\n";
	$smtp->to($to) 
		or die "Problème avec la commande RCPT TO\n";
	$smtp->data() 
		or die "Problème avec la commande DATA\n";
	$smtp->datasend($msg->as_string())
		or die "Problème d'envoi de message\n";
	$smtp->dataend()
		or die "Problème avec la fin d'envoi de message\n";
	$smtp->quit()
		or die "Problème avec la commande QUIT\n";

	# print ">>> Email envoye a $to pour $ical_name\n";

	return 1;
}

sub send_vcal_http
{
	my ($self, $groupe) = @_;
	# $chemin :: racine où enregistrer ...

	my $ical_name = $groupe->{'name'} . '.vcs';

	print "X-Powered-By: " . $self->{'version'} . "
Content-Type: text-calendar; charset=utf-8;
Content-Disposition: attachment; filename=$ical_name\n\n" . $groupe->{'vcs'};

	return 1;
}

sub open_all_branches
{
	my ($self, $root) = @_;
	
	my ($url, $tmp);
	my (@branches, @done);

	print  STDERR "\nLecture de l'arbre '$root'... \n";
	my $res = $self->exec_get($self->{'proto'} . "://" . $self->{'EDTRoot'} . "/etu/");
	return -1 if (not($res->decoded_content =~ /welcome.jsp/));

	# Permet de dérouler le premier noeud, "Etudiants"
	$self->openCategory($root);
	
	$url = $self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/tree.jsp";

	my $tree = $self->exec_get($url);
	my @tree = split /\n/, $tree->decoded_content;

	@branches = $self->get_all_ids(@tree);
	$tmp = pop(@branches);
	while($tmp)
	{
		print STDERR "Opening $tmp ... \n";
		$self->openBranch($tmp);
		push(@done, $tmp);
		print  STDERR "Reste :: @branches\n";
		print  STDERR  "Done  :: @done\n";
		$tree = $self->exec_get($url);
		@tree = split /\n/, $tree->decoded_content;
		@branches = $self->get_all_ids(@tree);
		$tmp = pop(@branches);
	}

	print  STDERR  "Found @branches branches ... \n";
}

sub get_all_ids
{
	my ($self, $groupe, @content) = @_;

	my @branches;
	foreach my $line (@content)
	{
		# On vient de trouver une branche fermée ...
		if($line =~ /<a href="javascript:openBranch\((\d+)\)"><img src=\"\/ade\/img\/plus.gif\"/)
		{
			push(@branches, $1);
		}
	}

	print STDERR "Fermés :: @branches\n";

	return @branches;
}

sub retrieve_ade_ids
{
	my ($self, $root) = @_;
	my %Groupes;
	my $tree;

	print  STDERR "Récupération de l'arbre '$root'... \n";

	# On ouvre toutes les branches, de telle sorte que le prochain chargement de tree.jsp nous ouvre les portes @#@
	$self->open_all_branches($root);
	$tree = $self->exec_get($self->{'proto'} . "://" . $self->{'EDTRoot'} . $self->{'EDTPath'} . "/standard/gui/tree.jsp");
	my @tree = split /\n/, $tree->decoded_content;
	
	my ($compteur, $tmpcompteur, $nom, $tmp, $level, $dernierlevel, $diff, $id, $parent, $precElem);
	my (@branches, @tmpstack);
	
	$tree = "";
	foreach(@tree)
	{
		$tmp = $_;
		$tmp =~ s/<\/SPAN>&nbsp;//g;
		$tmp =~ s/&nbsp;/\@/g;
		$tree .= $tmp . "\n"; 
	}
	
	
	my $data_file = $self->{'tmp'} . "/temp.html";
	# open(DAT, $data_file) || die("Could not open file!");
	open(DAT, ">$data_file") || die("Could not open file!");
	# my @tree = <DAT>;
	print DAT $tree;
	close(DAT);
	
	my $html = HTML::TreeBuilder->new();
	$html->parse_content($tree);

	$compteur = 0;
	$dernierlevel = 0;
	for (@{  $html->extract_links('a', 'img')  })
	{
		my($link, $element, $attr, $tag) = @$_;
		# print "CLASS='" .$element->attr_get_i('class'). "', PARENTCLASS='" .$element->parent()->attr_get_i('class'). "'\n";
		
		switch($element->parent()->attr_get_i('class'))
		{
		#
		# <DIV class="treeline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:openBranch(7287)"><img src="/ade/img/moins.gif"></a>
		#	<SPAN CLASS="treecross"><a href="javascript:checkBranch(7287, 'false')"><img src="/ade/img/smallplus.gif"></a></SPAN>
		#	<SPAN class="treebranch"><a href="javascript:checkBranch(7287, 'true')">Ancienne</a></SPAN>
		# </DIV>
		#
			case "treeline"
			{
				my $line = $element->parent()->as_text();
				$level = ($line =~ tr/@//);
				
				if($level > 0)
				{
					# print "Got this LINE :: '" . $line . "', level=$level, dernierlevel=$dernierlevel precElem=$precElem\n";
					if(($level < $dernierlevel) or ( ($level <= $dernierlevel) and ($precElem eq "treebranch" ) ) )
					{
						my $diff = int(($dernierlevel - $level)/3);
						# print "Changement de niveau ! diff=$diff ($dernierlevel - $level)\n";
						#print "Branches ::";
						foreach(@branches)
						{
						#	print "'$_';";
						}
						#print "\n";
						
						#print "Pop'ed :: ";
						for(my $i = 0; $i < $diff; $i++)
						{
							$tmp = pop(@branches);
						#	print "'$tmp';";
						}
						## Cas relou, cf "Rattrapages L2 suivi de Statistiques"
						if($precElem eq "treebranch")
						{
							$tmp = pop(@branches);
						#	print "'$tmp';";
						}
						#print "\n";
					}
					
					$dernierlevel = $level;
				}
				
				$precElem = "treeline";
			}
			
			case "treebranch"
			{
				# <SPAN class="treebranch"><a href="javascript:checkBranch(2720, 'true')">L1 -Histoire</a></SPAN>
				
				## On descend dans l'arbre ==> empiler + compteur+1
				# print "Got a BRANCH, pushing ... " . $element->as_text() . " level=$dernierlevel\n";
				push(@branches, $element->as_text());
				# print "Push'ed " . $element->as_text() . " [level=$dernierlevel] ... \n";
				$precElem = "treebranch";
			}
			
			case "treeitem"
			{
				# <SPAN CLASS="treeitem"><a href="javascript:check(2740, 'true');">Gr. A</a></SPAN>
				if($link =~ /javascript:check\((\d+),.*\);/)
				{
					$id = $1;
					$nom = $element->as_text();
					# print "Got an ITEM, $nom = $id\n";
					$nom =~ s/\s/_/g;
					@tmpstack = @branches;
					
					while(@tmpstack)
					{
						$tmp = pop(@tmpstack);
						$tmp =~ s/\s/_/g;
						# On doit construire la chaîne à l'envers ...
						$nom = $tmp . '/'. $nom;
						# print "Nom=='$nom'\n";
					}
					
					$Groupes{$nom} = $id;
				}
				
				$precElem = "treeitem";
			}
			
			
		}
	}
	
	$html->delete;
	return %Groupes;
}

sub ajout_date
{
	my ($self, $groupe) = @_;
	my $cur = $groupe->{'image_cur'};
	my $nxt = $groupe->{'image_nxt'};
	my $date_cur = $self->semaine_intervalle($self->calcul_semaine());
	my $date_nxt = $self->semaine_intervalle($self->calcul_semaine()+1);
	
	open (GIF, ">", $self->{'tmp'} . '/caca-cur.gif') or die "Impossible d'ouvrir : $!";
	binmode GIF;
	print GIF $cur;
	close GIF;

	open (GIF, ">", $self->{'tmp'} . '/caca-nxt.gif') || die;
	binmode GIF;
	print GIF $nxt;
	close GIF;
	
	my $gd_date = GD::Image->newFromGif($self->{'tmp'} . "/caca-cur.gif");
	if(defined($gd_date)) {
		my $marine = $gd_date->colorAllocate(0, 0, 128);
		my $wrapdate = GD::Text::Wrap->new($gd_date, color => $marine, text => $date_cur);
		$wrapdate->set_font('/usr/share/fonts/truetype/ttf-bitstream-vera/VeraBd.ttf', 22);
		$wrapdate->set(align => 'left');
		$wrapdate->draw(44, 22);
		$groupe->{'image_cur'} = $gd_date->png;
	}

	my $gd_date_nxt = GD::Image->newFromGif($self->{'tmp'} . "/caca-nxt.gif");
	if(defined($gd_date_nxt)) {
		my $marine_nxt = $gd_date_nxt->colorAllocate(0, 0, 128);
		my $wrapdate_nxt = GD::Text::Wrap->new($gd_date_nxt, color => $marine_nxt, text => $date_nxt);
		$wrapdate_nxt->set_font('/usr/share/fonts/truetype/ttf-bitstream-vera/VeraBd.ttf', 22);
		$wrapdate_nxt->set(align => 'left');
		$wrapdate_nxt->draw(44, 22);
		$groupe->{'image_nxt'} = $gd_date_nxt->png;
	}
}

sub exec_get
{
	my ($self, $url) = @_;
	my $req = HTTP::Request->new(GET => $url);
	return $self->exec_request($req);
}

sub exec_get_ref
{
	my ($self, $url, $ref) = @_;
	my $req = HTTP::Request->new(GET => $url);
	$req->header(Referer => $ref);
	return $self->exec_request($req);
}

sub exec_post
{
	my ($self, $url, $params, $content_type) = @_;
	my $req = HTTP::Request->new(POST => $url);
	$req->content_type($content_type);
	$req->content($params);
	return $self->exec_request($req);
}

sub exec_request
{
	my ($self, $req) = @_;
	my $ua = $self->{'browser'};

	$req->header('Connection' => 'Keep-Alive');
	$req->header('Accept-Encoding' => 'gzip,deflate');
	$req->header('Accept-Language' => 'fr-fr,fr;q=0.5');
	$req->header('Accept' => '*/*');
	$req->header('Host' => $req->uri->host);

	return $ua->request($req);
}

sub aff
{
	my ($self, $msg) = @_;

	if($self->{'print'}) {
		print $msg . "\n";
	}
}

sub utf
{
	my ($self, $texte) = @_;
	{
		if(defined($texte))
		{
			my $out = latin1($texte);
			return $out->utf8;
		} else {
			return "";
		}
	}
}

sub latin
{
	my ($self, $texte) = @_;
	{
		if(defined($texte))
		{
			my $out = utf8($texte);
			return $out->latin1;
		} else {
			return "";
		}
	}
}

1;
