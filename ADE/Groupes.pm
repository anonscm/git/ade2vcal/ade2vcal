#!/usr/bin/perl -w
#
# * Copyright (c) 2006-2008, Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr>
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *     * Redistributions of source code must retain the above copyright
# *       notice, this list of conditions and the following disclaimer.
# *     * Redistributions in binary form must reproduce the above copyright
# *       notice, this list of conditions and the following disclaimer in the
# *       documentation and/or other materials provided with the distribution.
# *     * Neither the name of the Polytech'Tours nor the
# *       names of its contributors may be used to endorse or promote products
# *       derived from this software without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr> ''AS IS'' AND ANY
# * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# * DISCLAIMED. IN NO EVENT SHALL Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr> BE LIABLE FOR ANY
# * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 

use strict;
package ADE::Groupes;

sub new
{
	my ($classe) = @_;
	my $self = {};
	
	bless($self, $classe);

	return $self;
	
	# $groupe contient :
	# $groupe->{'treeID'} : ID du groupe
	# $groupe->{'projID'} : ID du projet --> rempli par ADE::Main::getId
	# ---------{'width'}  : largeur de l'image
	# ---------{'height'} : hauteur de l'image
	# ---------{'image'}  : code "en dur" de l'image
}

sub ajouter_groupe
{
	my ($self, $annee, $groupe, $treeID, $width, $height, $conviName) = @_;

	$self->{'annees'}->{$annee}->{$groupe}->{'treeID'} = $treeID;
	$self->{'annees'}->{$annee}->{$groupe}->{'projID'} = 0;
	$self->{'annees'}->{$annee}->{$groupe}->{'width'}  = $width;
	$self->{'annees'}->{$annee}->{$groupe}->{'height'} = $height;
	$self->{'annees'}->{$annee}->{$groupe}->{'name'}   = $conviName;
	$self->{'annees'}->{$annee}->{$groupe}->{'image_cur'}  = '0';
	$self->{'annees'}->{$annee}->{$groupe}->{'image_nxt'}  = '0';

	return 1;
}

sub extract_annees
{
	my ($self) = @_;

	return $self->{'annees'};
};

sub extract_groupe
{
	my ($self, $annee, $groupe) = @_;

	return $self->{'annees'}->{$annee}->{$groupe};
};

sub extract_annee
{
	my ($self, $annee) = @_;
	
	return $self->{'annees'}->{$annee};
};

sub extract_groupe_annee
{
	my ($self, $annee, $groupe) = @_;
	
	return $annee->{$groupe};
};

1;
