#!/usr/bin/perl -w
# 
# * Copyright (c) 2006-2008, Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr>
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *     * Redistributions of source code must retain the above copyright
# *       notice, this list of conditions and the following disclaimer.
# *     * Redistributions in binary form must reproduce the above copyright
# *       notice, this list of conditions and the following disclaimer in the
# *       documentation and/or other materials provided with the distribution.
# *     * Neither the name of the Polytech'Tours nor the
# *       names of its contributors may be used to endorse or promote products
# *       derived from this software without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr> ''AS IS'' AND ANY
# * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# * DISCLAIMED. IN NO EVENT SHALL Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr> BE LIABLE FOR ANY
# * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 

use strict;
use FindBin;
use lib "$FindBin::Bin/"; # Permet de détecter le chemin du script, pour le lancer tranquillou de n'importe où

use ADE::Main;
use ADE::Groupes;

my $login = '';
my $passw = '';

my $smtp_server = 'smtp.univ.tld'; # Serveur SMTP à utiliser
my $smtp_from = 'vcal@univ.tld'; # Adresse FROM
my $smtp_to = 'vcal@univ.tld'; # Destinataire
my $smtp_subj = 'Calendrier '; # Titre qui sera complété.

# Si smtp_login est vide => SMTP non authentifié.
my $smtp_helo = 'smtp hello';
my $smtp_login = '';
my $smtp_passw = '';

my $width = 1024;
my $height = 768;

my $images_root = '';
my $ical_root = '';

my $edt = ADE::Main->new
	(
		'ADELogin'	=>	$login, # login ADE Campus
		'ADEPassw'	=>	$passw, # mot de passe ADE Campus
		'Proto'		=>	'http', # protocole http/https pour ADE
		'Auth'		=>	'CAS', # Authentification via ADE
		'CASUrlLogin'	=>	'https://cas.univ-tours.fr/cas/login',
		'CASUrlLogout'	=>	'https://cas.univ-tours.fr/cas/logout',
		'ImagesRoot'	=>	$images_root, # Où stocker les images ?
		'CalendarRoot'	=>	$ical_root, # Où stocker les calendriers ICS ?
		'SMTPServer'	=>	$smtp_server, # Adresse du serveur SMTP où envoyer
		'SMTPHello'	=>	$smtp_helo,  # Chaine EHLO à envoyer
		'SMTPFrom'	=>	$smtp_from, # Adresse de l'émetteur
		'SMTPTo'	=>	$smtp_to, # Adresse de destination
		'SMTPSubj'	=> 	$smtp_subj, # Sujet du mail, sera complété par le nom du .ics
		'SMTPLogin'	=>	$smtp_login, # Pour le SMTP Authentifié, login
		'SMTPPassw'	=>	$smtp_passw, # Pour le SMTP Authentifié, mot de passe
		'TempDir'	=>	'/tmp/', # Répertoire temporaire
		'TODO'		=>	"ecrire-ical", # Séparer par des ';', parmi : ecrire-image, ecrire-ical, envoyer-mail-ical
		'Print'		=>	1, # Affichage sur la sortie standard
		'NoFork'	=>	"true",
		'EDTRoot'	=>	"",
		'EDTPath'	=>	"",
		'MailHost'	=>	"",
	);

my $Univ = ADE::Groupes->new();

my $Profs = ADE::Groupes->new();
#	$Profs->ajouter_groupe("Profs", "DI/Profs/jean-yves.ramel", 366, $width, $height, "DI/Profs/jean-yves.ramel");
#	$Univ->ajouter_groupe("Univ", "DI/DI_5A/DI_5A_S10/DI_5A_Option_L&O", 10146 , $width, $height, "DI/DI_5A/DI_5A_S10/DI_5A_Option_L&O");

# %EPU est une école.
my %EPU = (
	"EPU"	=> $Univ,
	"Profs"	=> $Profs,
);

$edt->exec_ecole(%EPU);
