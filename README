ADE2vCal :
==========

 Cet outil permet de générer des calendriers au format iCal à utiliser dans n'importe quel outil de messagerie
 (testé avec succès dans Outlook, Evolution, Kontact/KMail et Sunbird), à partir des emplois du temps disponibles
 sur l'application ADE Campus.

Utilisation :
=============
 La génération se fait en exploitant la sortie texte proposée par ADE, et par une extraction « brutale » du contenu.
 On ne s'intéresse qu'aux feuilles de l'arbre des emplois du temps d'ADE, qui offrent la plus fine granularité.

 L'objet ADE::Main propose plusieurs paramètres :

	'ADELogin'      =>      $login, # login ADE Campus
	'ADEPassw'      =>      $passw, # mot de passe ADE Campus
	'Proto'		=>	'http', # protocole http/https pour ADE
	'Auth'		=>	'CAS', # Authentification via ADE
	'CASUrlLogin'	=>	'https://cas.univ-tours.fr/cas/login', # Adresse de connexion du CAS
	'CASUrlLogout'	=>	'https://cas.univ-tours.fr/cas/logout', # Adresse de déconnexion du CAS
	'ImagesRoot'    =>      $images_root, # Où stocker les images ?
	'CalendarRoot'  =>      $ical_root, # Où stocker les calendriers ICS ?
	'SMTPServer'    =>      $smtp_server, # Adresse du serveur SMTP où envoyer
	'SMTPHello'     =>      $smtp_helo,  # Chaine EHLO à envoyer
	'SMTPFrom'      =>      $smtp_from, # Adresse de l'émetteur
	'SMTPTo'        =>      $smtp_to, # Adresse de destination
	'SMTPSubj'      =>      $smtp_subj, # Sujet du mail, sera complété par le nom du .ics
	'SMTPLogin'     =>      $smtp_login, # Pour le SMTP Authentifié, login
	'SMTPPassw'     =>      $smtp_passw, # Pour le SMTP Authentifié, mot de passe
	'TempDir'       =>      '/tmp/', # Répertoire temporaire
	'TODO'          =>      "ecrire-ical", # Séparer par des ';', parmi : ecrire-image, ecrire-ical, envoyer-mail-ical, reponse-http
	'Print'         =>      1, # Affichage sur la sortie standard
	'NoFork'        =>      "true", # Ne pas forker. Par défaut, on fork un processus pour chaque calendrier
	'EDTRoot'       =>      "", # Serveur web pour EDT. Ex: emploidutemps.univ-tours.fr
	'EDTPath'       =>      "", # Chemin sur le serveur web. Ex: /ade
	'MailHost'      =>      "", # Domaine mail

Outils présents :
=================
 - config-univ.inc.pl : Sortie de l'exécution du script edt_get_ids.pl.
 - edt_get_ids.pl : Extraction de la liste complète des feuilles de l'arbre ADE.
 - edt_http-cgi.pl : Script CGI permettant de retourner « directement » le contenu de l'emploi du temps. Plus dynamique, mais assez consommateur en ressources. Il supporte une sorte de Rewrite URL, et propose, si pas de paramètre pour récupérer un emploi du temps précis, de générer la liste de tous les emplois du temps qu'il « connait ».
 - edt.pl : Le script principal, qui va générer et exporter tous les emplois du temps qu'on lui donnera en iCal.
 - edt-tests.pl : La copie du script, mais avec seulement quelques emplois du temps, à des fins de tests.
